import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class tempmain {

	public static void main(String[] args) {

		/*
		 * テスト用
		 */
		// 記入者に対するテストケース
		Person dev = new Person();

		dev.set_id(123);
		dev.set_grade("B3");
		dev.set_name("sukitaro");
		dev.set_semi("sukiyakii");


		// シートに対するテストケース
		Sheet sheet = new Sheet();

		sheet.set_author(dev);
		sheet.set_filled_date(new Date());

		// スケジュールに対するテストケース
		Schedule schedule = new Schedule();
		Calendar start = Calendar.getInstance();
		start.set(2018, 6, 1);
		Calendar end = Calendar.getInstance();
		end.set(2018, 8, 30);
		schedule.set_schedule(start, end, 7);
		sheet.set_schedule(schedule);


		// タスクに対するテストケース

		Task task = sheet.insert_new_task();

		task.taskdetail.set_taskname("プログラムの開発\nアクティブウィンドウの内容取得");
		task.taskdetail.set_person(dev);
//		task.progress
		task.perfection.set_perfection(100);
		task.devcomment.set_comment("上手く動作しない理由がChromeライブラリと開発中のソフトウェアの"
										+ "相性の性と考えていたが，バグと判明．");
		task.tutorcomment.set_comment("やっぱりバグでしたね．");


		Task task2 = sheet.insert_new_task();

		task2.taskdetail.set_taskname("プログラムの開発\n取得時間の同期（並列処理）");
		task2.taskdetail.set_person(dev);
//		task2.progress
		task2.perfection.set_perfection(100);
		task2.devcomment.set_comment("取得時間の部分が早くできたので，アクティブウィンドウの内容取得の機能を完成させた");
		task2.tutorcomment.set_comment("でかした");


		// シート表示テスト
		print_sheet(sheet);

	}

	static void print_sheet(Sheet sheet) {

		List<Task> tasklist = sheet.get_tasklist();
		Calendar start = sheet.get_schedule().get_start();
		Calendar end = sheet.get_schedule().get_end();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 E曜日");

		/*
		 * シートからタスクリストを取得し，それぞれ表示
		 */

		// 記入者と記入日について
		System.out.println("記入者：" + sheet.get_author().grade + " " + sheet.get_author().get_name());
		System.out.println("記入日：" + sheet.get_filleddate());
		// スケジュールについて
		System.out.println("スケジュール："
        					+sdf.format(start.getTime()) + " から " + sdf.format(end.getTime()) + " まで "
        					+ sheet.get_schedule().get_span() + " 日間隔");

		// タスクを１つずつ取り出し内容を表示
        int i=0;
		for(Task task : tasklist) {
			System.out.println("--------- "+ (++i) +"番目のタスク --------");

			System.out.println("実施内容：" + task.taskdetail.get_taskname());
			System.out.println("実施者：" + task.taskdetail.get_person().get_name());
			System.out.println("実施時期：" + "なうこーでぃんぐ");
			System.out.println("完成度：" + task.perfection.get_perfection());
			System.out.println("問題点・備考：" + task.devcomment.get_comment());
			System.out.println("チュータ意見：" + task.tutorcomment.get_comment());
		}


	}

}
