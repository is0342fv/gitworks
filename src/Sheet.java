

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Sheet {
	Person author = null;
	Date filled = null;
	List<Task> tasklist = new ArrayList<Task>();
	Schedule schedule = null; //スケジュールについてのデータ

	void set_author(Person author) {
		this.author = author;
	}

	Person get_author() {
		return author;
	}

	void set_filled_date(Date filled) {
		this.filled = filled;
	}

	Date get_filleddate() {
		return filled;
	}
	void set_schedule(Schedule schedule) {
		this.schedule = schedule;
	}
	Schedule get_schedule() {
		return schedule;
	}

	Task insert_new_task() {
		/*
		 * タスクをひとつ追加（挿入）する
		 */
		Task task = new Task(schedule);
		tasklist.add(task);
		return task;
	}

	List<Task> get_tasklist() {
		return this.tasklist;
	}

}
