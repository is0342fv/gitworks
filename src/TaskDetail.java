

public class TaskDetail {
	String taskname;
	Person person;

	void set_taskname(String taskname) {
		this.taskname = taskname;
	}
	void set_person(Person person) {
		this.person = person;
	}
	String get_taskname() {
		return taskname;
	}
	Person get_person() {
		return person;
	}
	
}
