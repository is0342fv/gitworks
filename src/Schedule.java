

import java.util.Calendar;

public class Schedule {
	/*
	 * もんだいあり
	 */
	Calendar start;
	Calendar end;
	int span;


	void set_schedule(Calendar start, Calendar end, int span) {
		this.start = start;
		this.end = end;
		this.span = span;
	}
	Calendar get_start() {
		return start;
	}
	Calendar get_end() {
		return end;
	}
	int get_span() {
		return span;
	}
}
